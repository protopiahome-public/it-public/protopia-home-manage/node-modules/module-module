import { GraphQLModule } from '@graphql-modules/core';

import { readFileSync } from 'fs';
import gql from 'graphql-tag';
import resolvers from './resolvers';
import db from '../../pe-core/server/db';
import MainModule from '../main-module';
import UserModule from '../user-module';

const schema = readFileSync(`${__dirname}/schema.graphqls`, 'utf8');

export default function (ctx) {
  const typeDefs = gql`
        ${schema}
    `;

  return new GraphQLModule({
    name: 'ModuleModule',
    resolvers,
    typeDefs,
    imports: [MainModule(ctx), UserModule(ctx)],
  });
}
