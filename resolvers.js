// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'module';

module.exports = {

  Mutation: {
    changeModule: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.input.author_id) {
        args.input.author_id = new ObjectId(args.input.author_id);
      }

      if (args._id) {
        return await query(collectionItemActor, { type: 'module', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'module', input: args.input }, global.actor_timeout);
    },
  },

  Query: {

    getModule: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'module', search: { _id: args.id } }, global.actor_timeout);
    },

    getModules: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'module' }, global.actor_timeout);
    },
  },
};
